// Character.cpp
#include "Character.h"
#include <iostream>
#include <algorithm> 

Character::Character(const std::string& name, int health) : name(name), health(health) {}

void Character::TakeDamage(int damage) {
    health -= damage;
    std::cout << name << " takes " << damage << " damage." << std::endl;
}

void Character::AddItem(const Item& item) {
    inventory.push_back(item);
}
