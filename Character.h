#pragma once
#include <string>
#include <vector>
#include "Item.h"
#include <iostream>
class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    void AddItem(const Item& item);
};


void Character::TakeDamage(int damage) {
    health -= damage;
    std::cout << name << " takes " << damage << " damage." << std::endl;
}
