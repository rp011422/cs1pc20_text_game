// Area.cpp
#include "Area.h"
#include <fstream>
#include <sstream>
#include <iostream>

Area::Area() {
    // Constructor body, if needed
}

Area::~Area() noexcept {
    // Destructor implementation
    for (auto& pair : rooms) {
        delete pair.second;
    }
    rooms.clear();
}

void Area::AddRoom(const std::string& name, Room* room) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        delete it->second; // Prevent memory leak by deleting the existing room
    }
    rooms[name] = room; // Assign the new room
}

Room* Area::GetRoom(const std::string& name) const {
    auto it = rooms.find(name);
    return it != rooms.end() ? it->second : nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);

    if (room1 && room2) {
        room1->AddExit(direction, room2);
        std::string inverseDirection = GetInverseDirection(direction);
        room2->AddExit(inverseDirection, room1);
    } else {
        std::cerr << "Error: Attempting to connect non-existent rooms '" << room1Name << "' and '" << room2Name << "'" << std::endl;
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    std::string line;
    std::string roomName, description, direction, adjacentRoomName;

    if (!file) {
        std::cerr << "Unable to open map file: " << filename << std::endl;
        return;
    }

    while (getline(file, line)) {
        std::istringstream lineStream(line);
        getline(lineStream, roomName, '|');
        getline(lineStream, description, '|');

        Room* room = GetRoom(roomName);
        if (!room) {
            room = GetRoom(description);
            AddRoom(roomName, room);
        } else {
            room->SetDescription(description); // Assuming Room has a SetDescription method
        }

        while (getline(lineStream, direction, '|') && getline(lineStream, adjacentRoomName, '|')) {
            Room* adjacentRoom = GetRoom(adjacentRoomName);
            if (!adjacentRoom) {
                adjacentRoom = GetRoom(""); // Placeholder for description, to be filled when that room is parsed.
                AddRoom(adjacentRoomName, adjacentRoom);
            }
            room->AddExit(direction, adjacentRoom);
        }
    }
}

std::string Area::GetInverseDirection(const std::string& direction) const {
    static const std::map<std::string, std::string> inverseDirections{
        {"north", "south"}, {"south", "north"},
        {"east", "west"}, {"west", "east"},
        {"up", "down"}, {"down", "up"}
    };

    auto it = inverseDirections.find(direction);
    if (it != inverseDirections.end()) {
        return it->second;
    } else {
        std::cerr << "Error: Invalid direction '" << direction << "'" << std::endl;
        return "";
    }
}
