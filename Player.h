// Player.h
#pragma once
#include "Character.h"
#include "Room.h"

class Player : public Character {
private:
    Room* location; // Current room location of the player

public:
    Player(const std::string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation() const;
    void Move(const std::string& direction);
 bool HasItem(const std::string& itemName) const; // Check if the player has a specific item
};
