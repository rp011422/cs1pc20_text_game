// Player.cpp
#include "Player.h"
#include <iostream>
#include <algorithm> 

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

void Player::SetLocation(Room* room) {
    location = room;
}

Room* Player::GetLocation() const {
    return location;
}

void Player::Move(const std::string& direction) {
    if (location != nullptr) {
        Room* nextRoom = location->GetExit(direction);
        if (nextRoom != nullptr) {
            SetLocation(nextRoom);
            std::cout << "You move to " << nextRoom->GetDescription() << std::endl;
        } else {
            std::cout << "There is no way to go " << direction << " from here." << std::endl;
        }
    }
}

// Check if the player has a specific item
bool Player::HasItem(const std::string& itemName) const {
    return std::any_of(inventory.begin(), inventory.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    });
}

