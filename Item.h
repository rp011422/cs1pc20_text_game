
#pragma once
#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc);
    void Interact() const;
    std::string GetName() const;
    std::string GetDescription() const; 
};
