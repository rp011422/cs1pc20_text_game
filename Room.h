// Room.h
#pragma once
#include <string>
#include <vector>
#include <map>
#include "Item.h" // Assuming Item class declarations are in Item.h

class Room {
private:
    std::string name;
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
    std::string requiredItem; // The name of the item required to enter the room, if any

public:
    Room(const std::string& name, const std::string& desc, const std::string& requiredItem);
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    void AddExit(const std::string& direction, Room* room);
    Room* GetExit(const std::string& direction) const;
    void DisplayItems() const;
    std::string GetDescription() const;
    std::vector<std::string> GetExitDirections() const;
};

