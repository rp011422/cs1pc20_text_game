#include "Room.h"
#include <algorithm>
#include <iostream>

// Room class constructor
Room::Room(const std::string& roomName, const std::string& roomDescription, const std::string& requiredItemName)
    : name(roomName), description(roomDescription), requiredItem(requiredItemName) {
}

bool Room::CanEnter(const std::string& itemName) const {
    return requiredItem.empty() || requiredItem == itemName;
}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    items.erase(std::remove_if(items.begin(), items.end(), [&item](const Item& i) {
        return i.GetName() == item.GetName();
    }), items.end());
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

Room* Room::GetExit(const std::string& direction) const {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    }
    return nullptr;
}

void Room::DisplayItems() const {
    for (const auto& item : items) {
        std::cout << item.GetName() << " - " << item.GetDescription() << std::endl;
    }
}

std::string Room::GetDescription() const {
    return description;
}

std::vector<std::string> Room::GetExitDirections() const {
    std::vector<std::string> directionList;
    for (const auto& exitPair : exits) {
        directionList.push_back(exitPair.first);
    }
    return directionList;
}
