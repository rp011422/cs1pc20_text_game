Starting Chamber|You are in a small chamber, lit by the faint glow of crystals embedded in the walls.|east|Enchanted Forest
Enchanted Forest|An expansive forest teeming with magic. Trees whisper secrets of the ancient world.|west|Starting Chamber|north|Forgotten Ruins
Forgotten Ruins|The ruins of an ancient civilization, now reclaimed by nature.|south|Enchanted Forest|east|Shadow Lair
Shadow Lair|A dark and ominous lair that is said to be the source of The Shadow's power.|west|Forgotten Ruins|up|Chamber of Light
Chamber of Light|The legendary chamber where The Last Light is kept, the only hope for Andoria's salvation.|down|Shadow Lair
