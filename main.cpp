#include <iostream>
#include <string>
#include <vector>
#include "Area.h"
#include "Player.h"
#include "Room.h"
#include "Item.h"

// Function declarations for any additional helper functions you may want to define.
void InitializeGameWorld(Area& gameWorld, Player& player);

int main() {
    // Initialization of the game world and player.
    Area gameWorld;
    Player player("Adventurer", 100);

    // Setup the game with rooms and items.
    InitializeGameWorld(gameWorld, player);

    // The initial room where the player starts.
    Room* currentRoom = gameWorld.GetRoom("Starting Chamber");
    player.SetLocation(currentRoom);

    // Main game loop
    bool gameRunning = true;
    while (gameRunning) {
        // Display current location
        std::cout << "Current Location: " << currentRoom->GetDescription() << std::endl;

        // Display items in the room
        currentRoom->DisplayItems();

        // Display available exits
        std::cout << "Exits available: ";
        for (const std::string& direction : currentRoom->GetExitDirections()) {
            std::cout << direction << " ";
        }
        std::cout << std::endl;

        // Player choices
        std::cout << "What would you like to do?" << std::endl;
        std::cout << "1. Look around" << std::endl;
        std::cout << "2. Move to another room" << std::endl;
        std::cout << "3. Interact with an item" << std::endl;
        std::cout << "4. Quit Game" << std::endl;
        std::cout << "> ";

        // Get and process the player's choice
        int choice;
        std::cin >> choice;
        switch (choice) {
            case 1: // Look around
                std::cout << "You take a moment to observe your surroundings." << std::endl;
                break;
            case 2: { // Move to another room
                std::cout << "Where would you like to go? ";
                std::string direction;
                std::cin >> direction;
                Room* nextRoom = currentRoom->GetExit(direction);
                if (nextRoom) {
                    currentRoom = nextRoom;
                    player.SetLocation(currentRoom);
                    std::cout << "Moving to the " << direction << std::endl;
                } else {
                    std::cout << "There is no path leading " << direction << std::endl;
                }
                break;
            }
            case 3: // Interact with an item
                // Implementation of item interaction would go here
                break;
            case 4: // Quit Game
                std::cout << "Thank you for playing 'The Last Light of Andoria'!" << std::endl;
                gameRunning = false;
                break;
            default:
                std::cout << "Not a valid option. Please try again." << std::endl;
                break;
        }
    }
  // When the player attempts to move to another room
  std::string direction;
  std::cin >> direction;
  Room* nextRoom = currentRoom->GetExit(direction);
  if (nextRoom) {
      if (nextRoom->CanEnter(player.HasItem("Key")))) {
          // Move to the room
      } else {
          // Inform the player they need a specific item
          std::cout << "The door is locked. You need a key to enter." << std::endl;

        
        



void InitializeGameWorld(Area& gameWorld, Player& player) {
    // Create items
    Item ancientSword("Ancient Sword", "A relic sword imbued with a mysterious power, said to be effective against The Shadow.");
    Item mysticalKey("Mystical Key", "An ornate key glowing with energy, rumored to unlock the Chamber of Light.");
    Item enchantedScroll("Enchanted Scroll", "An ancient scroll containing knowledge of the lost spells of Andoria.");

    // Create rooms
    Room startingChamber("Starting Chamber", "You are in a small chamber, lit by the faint glow of crystals embedded in the walls. It's the very place your journey begins.");

    Room enchantedForest("Enchanted Forest", "An expansive forest teeming with magic. The trees seem to whisper secrets of the ancient world.");
    Room forgottenRuins("Forgotten Ruins", "The ruins of an ancient civilization, now reclaimed by nature. There's a mystical aura surrounding the remnants.");
    Room shadowLair("Shadow Lair", "A dark and ominous lair that is said to be the source of The Shadow's power. Darkness seeps from the very walls.");
    Room chamberOfLight("Chamber of Light", "The legendary chamber where The Last Light is kept, hidden from the malevolent forces seeking its power.");

    // Add items to rooms
    startingChamber.AddItem(enchantedScroll); // The enchanted scroll is found here
    forgottenRuins.AddItem(mysticalKey); // The mystical key is hidden among the ruins
    shadowLair.AddItem(ancientSword); // The ancient sword is stashed in the shadow lair

    // Add rooms to the game world
    gameWorld.AddRoom("Starting Chamber", &startingChamber);
    gameWorld.AddRoom("Enchanted Forest", &enchantedForest);
    gameWorld.AddRoom("Forgotten Ruins", &forgottenRuins);
    gameWorld.AddRoom("Shadow Lair", &shadowLair);
    gameWorld.AddRoom("Chamber of Light", &chamberOfLight);

    // Connect rooms
    startingChamber.AddExit("forest", &enchantedForest);
    enchantedForest.AddExit("back", &startingChamber);
    enchantedForest.AddExit("ruins", &forgottenRuins);
    forgottenRuins.AddExit("back", &enchantedForest);
    forgottenRuins.AddExit("lair", &shadowLair);
    shadowLair.AddExit("back", &forgottenRuins);
    shadowLair.AddExit("chamber", &chamberOfLight);
    chamberOfLight.AddExit("back", &shadowLair);

    // Optionally, set the initial player location if not done outside this function
    player.SetLocation(&startingChamber);
}

            return 0;
        }
