#include "Item.h"
#include <iostream>

// Item class constructor
Item::Item(const std::string& name, const std::string& desc)
    : name(name), description(desc) {}

// Interact with the item
void Item::Interact() const {
    std::cout << "You examine the " << name << ": " << description << std::endl;
}

// Get the name of the item
std::string Item::GetName() const {
    return name;
}

// Get the description of the item
std::string Item::GetDescription() const {
    return description;
}

// Definitions of specific items for "The Last Light of Andoria"
Item ancientSword("Ancient Sword", "A relic sword imbued with a mysterious power, said to be effective against The Shadow.");
Item mysticalKey("Mystical Key", "An ornate key glowing with energy, rumored to unlock the Chamber of Light.");
Item enchantedScroll("Enchanted Scroll", "An ancient scroll containing knowledge of the lost spells of Andoria.");
